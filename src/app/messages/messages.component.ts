import { Component, OnInit } from '@angular/core';
import { MessageService }    from '../message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  // this instance of the MessageService must be public because I intend to use it in the template
  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

}
