/*
  This service is designed to manage a cache of messages to be displayed at the bottom of the application.
  This service will be injected into HeroService, which is then injected into the Heroes component.
*/
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  messages: string[] = [];

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}