import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero'; // importing the Hero class to be instantiated
import { HeroService } from '../hero.service'; // import the hero service

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
// always export the component class so that it can be imported elsewhere, like in the AppModule file
export class HeroesComponent implements OnInit {

  // exposes the imported array for binding
  heroes: Hero[];

  // The parameter simultaneously defines a private heroService property and identifies it as a HeroService injection site
  constructor( private heroService: HeroService ) {}

  // called just after creating the component. primarily for initialization logic
  ngOnInit() {
    this.getHeroes();
  }
  
  // subscribes to the observable set up in the hero service
  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }

  // if the name is non-blank, passes the new hero name to the heroService as a Hero object
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  // deletes the hero associated with the clicked delete button
  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);  // updates the UI prior to actually deleting the hero from the server
    this.heroService.deleteHero(hero).subscribe(); // calls the delete hero method in the heroService

    /*
      If you neglect to subscribe(), the service will not send the 
      delete request to the server. As a rule, an Observable does 
      nothing until something subscribes.
    */
  }
}
