import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component'; // musti import a component in order to add a route to it

// routes associate a web address with a a component to be loaded in response
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }, // defines the default route, the root of the application
  { path: 'heroes', component: HeroesComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent } // the ":id" in the path indicates that :id is a placeholder for a specific hero id
];

@NgModule({   // initializes the router and starts it listening for address changes
  imports: [RouterModule.forRoot(routes)], // imports RouterModule and configures it with the routes in one step with forRoot()
  exports: [RouterModule] // exported so that it will be available througout the app
})
export class AppRoutingModule { }