import { Component } from '@angular/core';

// meta-data for the component
@Component({
  selector: 'app-root', // string used to refer to this component from elsewhere in the application
  templateUrl: './app.component.html', // HTML template used to display the component, usually an external file
  styleUrls: ['./app.component.css'] // CSS stylings for the component template, usually an external file
})
export class AppComponent {
  title = 'Tour of Heroes';
}
