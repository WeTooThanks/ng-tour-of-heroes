// a class defining a hero. exported to be used in other components
export class Hero {
    id: number;
    name: string;
}